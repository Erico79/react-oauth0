import React, { Component } from 'react';
import { CardSubtitle } from 'reactstrap';

export default class NotFound extends Component {
    render() {
        return (
            <div>
                <CardSubtitle>404!</CardSubtitle>
                Page not Found!
            </div>
        );
    }
}