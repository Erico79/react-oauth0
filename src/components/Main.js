import React, { Component } from 'react';
import { CardText, CardSubtitle, Button } from 'reactstrap';

export default class Main extends Component {
    render() {
        return (
            <div>
                <CardSubtitle>Main</CardSubtitle>
                <CardText>Do you want to see the secret area? <a href="/secret">Click here</a></CardText>
                
                {!this.props.auth.isAuthenticated() ?
                    <React.Fragment>
                        <hr /><p>Please login first</p>
                        <Button color="success" onClick={this.props.auth.login}>Log In</Button>
                    </React.Fragment> : null
                }
            </div>
        );
    }
}