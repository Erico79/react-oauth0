import React, { Component } from 'react';
import {
  Container,
  Button,
  Row,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardBody,
  CardTitle
} from 'reactstrap';

import './App.css';

import Main from './components/Main';
import Secret from './components/Secret';
import NotFound from './components/NotFound';
import Callback from './components/Callback';

class App extends Component {
  state = {
    isOpen: false
  };

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { auth }  = this.props;

    let mainComponent = "";
    switch(this.props.location) {
      case "":
        mainComponent = <Main {...this.props}/>;
        break;
      case "secret":
        mainComponent = auth.isAuthenticated() ? <Secret /> : <NotFound />;
        break;
      case "callback":
        mainComponent = <Callback />;
        break;
      default:
        mainComponent = <NotFound />;
    }
    return (
      <Container>
        <div>
          <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">React & OAuth0</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavLink href="/">Main</NavLink>
                <NavItem>
                  {!auth.isAuthenticated() ? <Button outline color="success" onClick={this.props.auth.login}>Log In</Button> :
                  <Button outline color="danger" onClick={auth.logout}>Log Out</Button>}
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
          <Row>
            <Card className="col-6 offset-3 mt-4">
              <CardBody>
                <CardTitle>Welcome, {this.props.name}!</CardTitle>
                {mainComponent}
              </CardBody>
            </Card>
          </Row>
        </div>
      </Container>
    );
  }
}

export default App;
