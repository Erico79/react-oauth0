import React, { Component } from 'react';
import { CardSubtitle } from 'reactstrap';

export default class Secret extends Component {
    render() {
        return (
            <div>
                <CardSubtitle>Secret</CardSubtitle>
                This is a super secret area.
            </div>
        );
    }
}
